/**
 * Created by kgr on 6/1/16.
 */

'use strict';

/*jslint browser: true*/
/*global $, Backbone, Flickr, _*/

// - models

var StaticPageItem = Backbone.Model.extend({
  defaults: {
    title: 'Celebrity web page',
    subtitle: 'Warszawa, PL',
    loremIpsum: 'Lorem ipsum dolor sit amet, consectetur adipiscing' +
    'elit, sed do eiusmod tempor incididunt ut labore et dolore magna ' +
    'aliqua. Ut enim ad minim veniam, quis.'
  },
  flipTitle: function() { // testowa metoda modelu
    this.set({title: this.get('title').split('').reverse().join('')});

    // ... this.save(); // w przypadku gdybyśmy mieli strony oparte o serwis restowy
  }
});

var FlickrPhotoItem = Backbone.Model.extend({});

// - collections

var FlickrPhotoList = Backbone.Collection.extend({
  model: FlickrPhotoItem,
  initialize: function () {
    // this.on('remove', this.hideModel);
  },
  fetch: function(options) { // tu zdecydowałem się nadpisać metodę fetch ponieważ z powodu zagnieżdzonej struktury odpowiedzi z flickra nie mogłem zastosowac url: ... no chyba, że ... ;)
    var listView = this;
    var flickr = new Flickr({
      api_key: '20c8c8c43391ff5f564bec148ad30997'
    });

    flickr.photos.search({
      // text: 'marilyn+monroe',
      tags: 'marilyn+monroe', // TODO via params
      sort: 'date-posted-desc',
      per_page: 9 // TODO via params
    }, function(err, result) {
      if (err) {
        throw new Error(err);
      }

      listView.reset(result.photos.photo);

      if (listView.length >=3) {
        console.log(listView.at(2)); // mały test
      }
    });
  },
  getAllPhotoIds: function() { // mały test
    return this.map(v => v.get('farm'));
  }
});

// - mixins

var TestMixin = {
  commonDump: function () {
    console.log('test mixin dump:');
    for (var k in this) {
      if ((/string|number|boolean/).test(typeof this[ k ])) {
        console.log('- ' + k + ': ' + this[k])
      }
    }
  }
}

// - views

var StaticPageView = Backbone.View.extend(_.extend({}, TestMixin, {
  model: StaticPageItem,
  tagName: 'article',
  className: 'richtext',
  template: _.template($("#staticPageTmpl").html()), // ładowanie templata ze <script... >
  initialize: function() {
    console.log('initializing view');

    this.model.on('change', this.render, this);
    // this.model.on('destroy', this.remove, this); ... this.$el.remove()

    this.commonDump(); // test mixina
  },
  events: {
    'change': function() {
      this.render();
    },
    'dblclick h2': function() { // mój mały test
      this.model.flipTitle();
      // this.render(); // oops niewłaściwy sposób odświeżania widoku ... poprawny dodałem w: initialize ... this.model.on('change' ...
    }
  },
  render: function() {
    var attributes = this.model.toJSON();

    if (!attributes.content) {
      attributes.content = // fake content data
        '<p>' + attributes.loremIpsum + '</p>' +
        '<blockquote>' + attributes.loremIpsum + '</blockquote>' +
        '<p>' + attributes.loremIpsum + '</p>';
    }

    this.$el.html(this.template(attributes));
  }
}));

var FlickrPhotoView = Backbone.View.extend({
  tagName: 'li',
  template: _.template('<a href="<%= photourl %>" target="_blank"><img src="<%= thumburl %>"/></a>'),
  events: {
    // 'click': 'openFlickr' // zrezygnowałem z tego sposobu nawigowania do flickra na rzecz prostego przepuszczania klików na nie-wewnętrzne zasoby w $(document)
  },
  initialize: function() {
  },
  render: function() {
    // TODO configurable: q
    var thumburl = 'https://farm' + this.model.get('farm') + '.staticflickr.com/' + this.model.get('server') + '/' + this.model.get('id') + '_' + this.model.get('secret') + '_q.jpg';
    var photourl = 'https://www.flickr.com/photos/' + this.model.get('owner') + '/' + this.model.get('id');

    // var attributes = this.model.toJSON(); // porzuciłem to żeby nie kleić tych urli w template
    // this.$el.html(this.template(attributes));

    this.$el.html(this.template({
      thumburl, // Property Shorthand,
      photourl
    }));
  }
});

var FlickrPhotoListView = Backbone.View.extend({
  // el: '#app',
  tagName: 'ul',
  className: 'listview',
  initialize: function() {
    this.collection.on('reset', this.render, this);
    // this.collection.on('add', this.addOne, this); // na razie niepotrzebne ale wymagałoby dodania osobnej metody która zaktualizowała by DOM

    if (this.collection.length === 0) {
      console.log('initializing list view'); // TODO prevent reinicjalization
      this.collection.fetch();
    } else {
      this.render();
    }
  },
  render: function() {
    console.log('photo ids: ' + this.collection.getAllPhotoIds().join(', ')); // test

    this.collection.forEach(photo => {
      var flickrPhotoView = new FlickrPhotoView({
        model: photo
      });

      flickrPhotoView.render();

      this.$el.append(flickrPhotoView.el);
    });

    $('#app').html(this.$el);
  }
});

// - router && lift off

$(function() {
  var appRouter = new (Backbone.Router.extend({ // Backbone.Router({
    routes: {
      '': 'indexPage',
      'gallery': 'galleryPage',
      'hello/:what': 'hello'
    },
    initialize: function () {
      this.indexPage = new StaticPageView({model: new StaticPageItem({
          title: 'Marilyn Monroe',
          subtitle: 'Poznań, PL',
      })});

      this.flickrPhotoList = new FlickrPhotoList();

      Backbone.history.start({pushState: true});
    },
    indexPage: function() {
      this.indexPage.render();

      $('#app').html(this.indexPage.$el);
    },
    galleryPage: function() {
      $('#app').html(''); // czyszczę, żeby pokazać info o ładowaniu. API flicka potrafiło czasami odpowiadać po 8s!
      var flickrPhotosPage = new FlickrPhotoListView({
        collection: this.flickrPhotoList
      });
    },
    'hello': function(what) { // test
      alert('hello ' + what);
    }
  }));

  $(document).on('click', 'a', function(e){
    var href = $(e.currentTarget).attr('href');
    if (href.match(/^(#|\/)/)) { // Przechwytuję tylko wewnętrzne clicki
      e.preventDefault();

      appRouter.navigate(href, true);
    }
  });
});

